package com.security.innovation.service;

import org.springframework.stereotype.Service;

@Service
public class ApiUrlConverter {

	private static final String UsedChar = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private final char[] usedCharacters = UsedChar.toCharArray();
    private final int base = usedCharacters.length;

    public String LongToShortURL(long url){
    	StringBuffer shorturl = new StringBuffer(); 

        if(url == 0) {
            return String.valueOf(usedCharacters[0]);
        }

        while (url > 0) {
        	shorturl.append(usedCharacters[(int) (url % base)]);
        	url = url / base;
        }

        return shorturl.reverse().toString();
    }

    public long ShortToLongURL(String url) {
    	char[] characters = url.toCharArray();
        int length = characters.length;

        long longURL = 0;

        //counter is used to avoid reversing input string
        long counter = 1;
        for (int i = 0; i < length; i++) {
        	longURL += UsedChar.indexOf(characters[i]) * Math.pow(base, length - counter);
            counter++;
        }
        return longURL;
    }
}
