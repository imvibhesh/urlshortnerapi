package com.security.innovation.service;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.security.innovation.dto.UrlDTO;
import com.security.innovation.entity.Url;
import com.security.innovation.exception.EntityNotFoundException;
import com.security.innovation.repository.ApiUrlRepository;

@Service
public class ApiUrlService {

	@Autowired
	private ApiUrlRepository apiUrlRepository;
	@Autowired
    private ApiUrlConverter urlConverter;

    public String convertToShorterUrl(UrlDTO request) {
        Url url = new Url();
        url.setLongUrl(request.getLongUrl());
        url.setCreatedDate(LocalDateTime.now());
        
        Url entity = apiUrlRepository.save(url);

        return urlConverter.LongToShortURL(entity.getId());
    }

    public String convertToLongUrl(String shortUrl) {
        long longUrl = urlConverter.ShortToLongURL(shortUrl);
        Optional<Url> entity = Optional.ofNullable(apiUrlRepository.findById(longUrl)).get();
        if(entity.isPresent()) {
        	return entity.get().getLongUrl();
        }
        else {
        	throw new EntityNotFoundException("There is no entity with " + shortUrl);
        }

    }
}
