package com.security.innovation.exception;

import java.time.LocalDateTime;

//Step-12
public class ExceptionResponse {

	private LocalDateTime timestamp;
	private Integer status;
	private String errorMessage;
	private String details;
	public ExceptionResponse(LocalDateTime timestamp, Integer status, String errorMessage, String details) {
		super();
		this.timestamp = timestamp;
		this.status = status;
		this.errorMessage = errorMessage;
		this.details = details;
	}
	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public Integer getStatus() {
		return status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public String getDetails() {
		return details;
	}
	
}
