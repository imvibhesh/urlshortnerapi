package com.security.innovation.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

//Step-11
@ResponseStatus(code = HttpStatus.NOT_FOUND) //added after getting 500 as exception
public class EntityNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EntityNotFoundException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

}
