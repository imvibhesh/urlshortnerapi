package com.security.innovation.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.security.innovation.entity.Url;

public interface ApiUrlRepository extends JpaRepository<Url, Long>{
	
}
