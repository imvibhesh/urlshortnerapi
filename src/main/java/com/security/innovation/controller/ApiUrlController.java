package com.security.innovation.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JacksonException;
import com.security.innovation.dto.UrlDTO;
import com.security.innovation.service.ApiUrlService;


@RestController
@RequestMapping("/api/v1/")
public class ApiUrlController {
	
	@Autowired
	private ApiUrlService urlService;


    @RequestMapping(value= "create-url", method= RequestMethod.POST)
    public String convertToShortUrl(@Valid @RequestBody UrlDTO request) {
        return urlService.convertToShorterUrl(request);
    }

    @RequestMapping(value= "{shortUrl}", method = RequestMethod.GET, produces = "application/html")
    @Cacheable(value = "urls", key = "#shortUrl", sync = true)
    public ResponseEntity<Object> getShortUrlAndRedirect(@PathVariable String shortUrl) throws URISyntaxException  {
        String url = urlService.convertToLongUrl(shortUrl);
        URI uri = new URI(url);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(uri);
        return new ResponseEntity<>(httpHeaders, HttpStatus.MOVED_PERMANENTLY);

    }
}
