package com.security.innovation.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.security.innovation.CustomUrlShortenerApiApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=CustomUrlShortenerApiApplication.class)
public class ApiUrlConverterTest {
	
	@Autowired
	private ApiUrlConverter converter;

	@Test
    public void LongToShortURL_Test1() {
        assertEquals("f", converter.LongToShortURL(5));
    }

    @Test
    public void LongToShortURL_Test2() {
        assertEquals("d", converter.LongToShortURL(186));
    }

    @Test
    public void ShortToLongURL_Test3() {
        assertEquals(5, converter.ShortToLongURL("f"));
    }
    @Test
    public void ShortToLongURL_Test4() {
        assertEquals(186, converter.ShortToLongURL("d"));
    }
}
