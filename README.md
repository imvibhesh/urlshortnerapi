**Intsruction for running the Application in your local machine:**
> 1. Run the application as Spring Boot Application in IDE
> 2. Open the postman and hit the url: http://localhost:8080/api/v1/create-url as POST request with body as 
> {
>  "longUrl": "google.com",
>  "createdDate": "2020-06-07T08:20:51.483" 
> } 
> and Now check the h2-console[http://localhost:8080/h2-console/login.jsp] for table data. 
> And output on postman console will be [depend on input number], take first input output as b.
> 3. Then hit the url: http://localhost:8080/api/v1/b as GET request and this will produce output as 
>  google.com on postman console. Or we can hit this url in any browser for redirect to google.com.

> Algorithm Used : tinyUrl
